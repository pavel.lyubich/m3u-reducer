from utils import constants
from raw_modifier.channel_modifier import modify_m3u_list
from ipytv_modifier.ipytv_channel_modifier import create_updated_channel_list

if __name__ == '__main__':
    modify_m3u_list(constants.input_filename, constants.to_keep_chan_list)
    create_updated_channel_list(constants.input_filename, constants.to_keep_chan_list)
