# Given a full playlist, end result will produce a new subset playlist file with only the channels in this number list
to_keep_chan_list = [1, 4, 6, 7, 12, 31]    # example list

# Input filename location or place it in the root folder
input_filename = '../[YOUR_FILE_IN_ROOT_FOLDER].m3u'

# Used to replace the original filename with the new when using raw modifier.
# File extension is taken from input filename
new_filename = 'NEW_FILENAME_FOR_NEW_PLAYLIST'

# Used to replace the original filename with the new when using ipytv modifier.
# File extension is taken from input filename
new_ipytv_filename = "NEW_FILENAME_FOR_NEW_PLAYLIST_IPYTV"
