def append_to_filename(filename, appender):
    return "{0}_{2}.{1}".format(*filename.rsplit('.', 1) + [appender])


def replace_filename(filename, appender):
    return "{2}.{1}".format(*filename.rsplit('.', 1) + [appender])


def get_0_based_channel_list(channel_list):
    print(f'Input list original | {channel_list}\n')
    channel_list_0_indexed = [x - 1 for x in channel_list]
    print(f'Input list 0-indexed | {channel_list_0_indexed}\n')

    return channel_list_0_indexed


def save_playlist_to_file(filename, data_str):
    print(f'Writing Data to a File {filename}\n')
    with open(filename, 'w') as output_file:
        output_file.writelines(data_str)
