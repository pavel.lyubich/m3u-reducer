from utils.utils import replace_filename, append_to_filename, get_0_based_channel_list


def modify_m3u_list(filename, new_filename, channel_list):
    print(f'Reading File, {filename}')
    read_and_group_lines(filename, new_filename, channel_list)


def read_and_group_lines(filename, new_filename, channel_list):
    result_map = {}
    final_channel_list = []

    channel_list_0_indexed = get_0_based_channel_list(channel_list)

    with open(filename, 'r') as file:
        lines = file.readlines()

    print('Adding Channels\n')
    for index in range(1, len(lines), 3):
        key = (index // 3)  # Using integer division to get the index as the key
        value = lines[index:index + 3]
        if key in channel_list_0_indexed:
            key += 1
            print(f'Adding Channel | {key} with file line index | {index}')
            print(value)
            final_channel_list.append(key)
            result_map[key] = value

    output_filename = replace_filename(filename, new_filename)
    result_list_filename = append_to_filename(filename, 'results')

    print('Writing Channels\n')
    # Save the results to another file
    with open(output_filename, 'w') as output_file:
        output_file.writelines(lines[0])
        for key, value in result_map.items():
            print(f'Writing Channel, {key}')
            print(value)
            output_file.writelines(value)

    # Save the results list to another file
    with open(result_list_filename, 'w') as output_file:
        original_number_str = ','.join(map(str, channel_list))
        numbers_str = ','.join(map(str, final_channel_list))
        output_file.writelines(original_number_str)
        output_file.writelines('\n')
        output_file.writelines(numbers_str)
