from ipytv import playlist
from ipytv.playlist import M3UPlaylist

from utils import utils


def create_updated_channel_list(filename, new_filename, channel_list):
    pl = playlist.loadf(filename)
    print(f'Input Playlist\n{pl}')

    pl_new = M3UPlaylist()
    pl_new.add_attributes(pl.get_attributes())

    channel_list_0_indexed = utils.get_0_based_channel_list(channel_list)

    for channel_num in channel_list_0_indexed:
        pl_new.append_channel(pl.get_channel(channel_num))

    print(f'New Playlist\n{pl_new}')
    utils.save_playlist_to_file(utils.replace_filename(filename, new_filename), pl_new.to_m3u_plus_playlist())
